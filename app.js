const express = require("express");
const app = express();
const port = 3000;
const path = require("path");
const usersRouter = require('./routes/users');
const bcrypt = require('bcrypt');
const fs = require('fs');
const jwt = require('jsonwebtoken');



  
  

//  routes
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
//css & js
app.use(express.static(path.join(__dirname, "public")));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");


app.get('/', (req, res) => {
    res.render('index');
})

app.get('/index2', (req, res) => {
    res.render('index2');
})


let users = [
    {
      username: "alfian",
      password: "123",
    },
    {
      username: "Hafizh",
      password: "345",
    },
    {
      username: "Christo",
      password: "456",
    },
  ];


  
  const middleware = (req, res, next) => {
    req.body.sender = "0.0.0.0";
    next();
  };
  app.use(middleware);
  
  app.get('/login', (req, res) => {
    res.render('login');
  })
  app.get('/register', (req, res) => {
    res.render('register');
  })
  
  app.get("/login", (req, res) => {
    
    if (users.length === 0) {
      res.status(500).json({
        status: "Data not available",
      });
    }
    res.json(users);
  });
  
  app.get("/:username", (req, res) => {
    const value = users.find((item) => {
      console.log(item);
      return item.username === req.params.username;
    });
    if (!value) {
      res.status(500).json({
        status: "Data not available",
      });
    }
    res.json(value);
  });
  
  //Register
  app.post("/", (req, res) => {
    const { username, password } = req.body;
    
    const newUser = {
      username,
      password,
    };
  
    users.push(newUser);
  
    console.log("insert to db berhasil", users);
    let user = users.find((item) => item.username == username);
    console.log("usernya adalah :", user);
    res.render('login');
  });

  //Login
  app.post("/login", (req, res) => {
    console.log(req.body);
    console.log(users);
    const { username, password } = req.body;
    let user = users.find((item) => item.username == username);
    console.log("user : ", user.username);
    console.log("username : ", username);
    try {
      const parsedPassword = parseInt(password);
  
      if (typeof username !== "string") {
        throw new Error("Username is not a string");
      } else if (typeof parsedPassword !== "number") {
        throw new Error("Password is not a number");
      } else if (username !== user.username) {
        throw new Error("Tidak ada username tersebut");
      }
  
      if (username.length > 20 || password.length !== 3) {
        throw new Error("Username/Password length is not correct");
      } else {
        // insert to db
        console.log("Berhasil login", users);
        res.render('index');
      }
    } catch (err) {
      console.log(err);
      res.status(400).send(err.message);
    }
  });
  
  
  app.put("/:username", (req, res) => {
    const { username } = req.params;
  
    console.log(username);
  
    // Buat object yang akan di edit-kan ke data sekarang
    let objectToAdd = { username, password };
    console.log("objectToAdd", objectToAdd);
  
    // Cari id data yang mau di edit
    let value = users.find((item) => item.username == username);
    console.log("value to edit", value);
  
    // Edit datanya
    value = { ...value, ...objectToAdd };
    console.log("telah di edit menjadi", value);
  
    // Buat array baru yang isinya :
    // Jika id === yang akan di edit : object yang telah diubah
    // Jika id !== data yang akan di edit : object asli yang tidak berubah
    users = users.map((item) => (item.username === value.username ? value : item));
  
    // Kembalikan
    res.status(200).json(users);
  });
  
  app.delete("/:username", (req, res) => {
    const { username } = req.params;
  
    users = users.filter((item) => item.username !== req.params.username);
    res.status(200).json(users)
  });
  
  
  




app.use('/users', usersRouter);

app.get('*', function (req, res, next) {
    res.status(404).send('Errrooor 404');
    next()
  });

app.get('*', function (req, res, next) {
    res.status(404).send('Errrooor 404');
    next()
  });

app.post('*', function (req, res, next) {
    res.status(404).send('Errrooor 404');
    next()
  });


  

  

// port 3000
app.listen(port, () =>{
    console.log('hit ke http://localhost:${port}');
});

