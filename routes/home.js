const express = require('express')
const app = express()

// Router
const gameRouter = require('./game')
const loginRouter = require('./login')

// JSON
const userJSON = require('../users.json')
console.log(userJSON)

app.use(express.json())
app.use(express.urlencoded({ extended: true }))


// setting ejs
app.set('view engine', 'ejs')

app.use(express.static('public'))

app.get('/', (req,res) => {
    res.status(200).render('home')
})

app.use('/game', gameRouter)

app.use('/login', loginRouter)
app.use(express.static('public'))

app.get('/users', (req,res) => {
    res.status(200).json(userJSON)
})

// app.get('/:username', (req, res) => {
//     const {username} = req.params
//     //console.log("Username User : ", username)
//     //console.log(userJSON.find((users) => users.username === username))

//     //res.json(userJSON.find((username) => userJSON.username === username))
// }) 

// app.post('/:username', (req, res) => {
//     //console.log("Ini request body : ", req.body)
//     //res.send(req.body) 

//     // const { email, password } = req.body

//     //liat video rekaman 5, menit 50 an

//     // users
//     res.render('home')
// })

app.get('*', (req,res) => {
    res.status(404).send('404 NOT FOUND')
})

app.listen(3000, () => {
    console.log(3000)
})
